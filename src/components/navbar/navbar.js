import React, { Component } from "react";
import { Navbar, Nav, NavDropdown, NavbarBrand } from "react-bootstrap";
import logo from "./logo.png";


class Navigation extends Component {
    render() {
        return (
            <Navbar className="mb-5" bg="primary" variant="dark">
                <Navbar.Brand href="/">
                    <img
                        alt=""
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{" "}
                    Walmart Labs Issues Browser
                </Navbar.Brand>
            </Navbar>
        );
    }
}

export default Navigation;
