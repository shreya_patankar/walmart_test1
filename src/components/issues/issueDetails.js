import React from "react";
import { Component } from "react";
import {
    Card,
} from "react-bootstrap";
import { Container } from "react-bootstrap";

class IssuesDetails extends Component {

    issue={};
    constructor(props) {

        super(props);
        // console.log(this.props.location);
        this.issue = this.props.location.issueDetails;
    }

    componentDidMount() {

    }

    render() {

        return (
            <div>
                <Container>
                    <Card border="primary">
                        <Card.Header>Issue Details</Card.Header>
                        <Card.Body>
                            <Card.Text>
                                Issue Id : {this.issue.id}
                            </Card.Text>
                            <Card.Text>
                                Created At : {this.issue.created_at}
                            </Card.Text>
                            {this.issue.body.length>0 && <Card.Text>
                                Description : {this.issue.body}
                            </Card.Text>}
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        );
    }
}

export default IssuesDetails;

