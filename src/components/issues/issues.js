import React from "react";
import { Component } from "react";
import { Link } from "react-router-dom";
import {
    Card,
    Row
} from "react-bootstrap";

import { apiUrl } from "../../config";
import axios from "axios";
import { Container, Pagination } from "react-bootstrap";
class Issues extends Component {

    constructor(props) {

        super(props);

        this.state = {
            issues: [],
            startIndex: 0,
            endIndex: 9
        }
    }

    componentDidMount() {
        this.getIssues();
    }

    getIssues = () => {

        axios.get(apiUrl)
            .then((res) => {
                // console.log(res)
                this.setState({ issues: res.data,
                                     pageCount: Math.ceil(res.data.length / 10),});
            });
    };

    handleNextPageChange = (e) => {
        console.log(e);
        this.setState({startIndex : Number(e.target.text) * 10});
        this.setState({endIndex : Number(e.target.text) * 10 + 9});


    }


    render() {

        let pageNumbers = []
        for (let number = 0; number < this.state.issues.length/10; number++) {
            pageNumbers.push(
                <Pagination.Item key={number} >
                    {number}
                </Pagination.Item>
            );
        };

        const paginationBasic = (
            <div>
                <Container>
                    <Pagination className="float-right pt-2" onClick={ (e) => this.handleNextPageChange(e) }>{pageNumbers}</Pagination>
                </Container>
            </div>
        );

        const currentPageIssues = this.state.issues.slice(this.state.startIndex, this.state.endIndex + 1);

        let list = currentPageIssues.map((item, index) => (
            <Container>
                <Card
                    bg="light"
                    className="mt-3 border border-primary"
                    key={item.id}
                >
                    <Card.Body>
                        <Link to={
                            {
                                pathname:   `/${item.id}`,
                                issueDetails: item
                            }
                        }>
                            {item.title}
                        </Link>
                        <Card.Subtitle className="mb-2 text-muted">#{item.number} {item.state}</Card.Subtitle>
                    </Card.Body>
                </Card>
            </Container>

        ));


        return (
                <div>
                    <Container fluid>

                        <Row>{list}</Row>
                        {paginationBasic}
                    </Container>

                </div>
        );
    }
}

export default Issues;

