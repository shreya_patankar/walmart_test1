import React, { Component } from "react";
import { Route } from "react-router-dom";
import "../App.css";
import Navigation from "./navbar/navbar";
import Issues from "./issues/issues"
import IssueDetails from "./issues/issueDetails"
class Home extends Component {
    render() {
        return (
            <div>
                <Route path="/" component={Navigation} />
                <Route exact path="/" component={Issues} />
                <Route exact path="/:id" component={IssueDetails} />
            </div>
        );
    }
}
//export Home Component
export default Home;
