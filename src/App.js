
import React, { Component } from "react";
import "./App.css";
import Main from "./components/main";
import { BrowserRouter } from "react-router-dom";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <div>
            {/* App Component Has a Child Component called Main*/}
            <Main />
          </div>
        </BrowserRouter>
    );
  }
}

//Export the App component so that it can be used in index.js
export default App;
